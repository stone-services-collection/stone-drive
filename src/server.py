# Stone Drive

import os
import json
import secrets
from os.path import expanduser
from flask import request, redirect, url_for, session
from flask import Flask, render_template, jsonify, send_from_directory

# SET APP VARIABLE
app = Flask(__name__)
group_list = []

# ----------------------------------------------------------#
# INITIAL SETUP                                             #
# ----------------------------------------------------------#

# CREATE APP FOLDERS
HOMEDIR = expanduser("~")
APP_FOLDER = HOMEDIR + "/Latch/S-Drive/"
CLOUD_FOLDER = HOMEDIR + "/Latch/S-Drive/cloud/"
SETTINGS_FOLDER = HOMEDIR + "/Latch/S-Drive/settings/"

try: os.mkdir(HOMEDIR + "/Latch")
except: pass
try: os.mkdir(APP_FOLDER)
except: pass
try: os.mkdir(CLOUD_FOLDER)
except: pass
try: os.mkdir(SETTINGS_FOLDER)
except: pass

# Create logs.txt
try: 
    if (os.path.isfile(SETTINGS_FOLDER+"logs.txt")):
        pass
    else:
        with open(SETTINGS_FOLDER+"logs.txt", 'w') as f:
            f.write("--- LOGS ENTRYS ---")
except: pass

# Create config.json file
try: 
    if (os.path.isfile(SETTINGS_FOLDER+"config.json")):
        pass
    else:
        with open(SETTINGS_FOLDER+"config.json", 'w') as f:
            temp = {"username": "stone", "password": "stone"}
            f.write(json.dumps(temp))
except: pass

# ----------------------------------------------------------#
# PRIMARY ROUTES                                            #
# ----------------------------------------------------------#
@app.route("/")
def index():
    return redirect('/login')

@app.route('/login', methods=["GET", "POST"])
def login():
    if (request.method == 'POST'):
        login_uname = request.form["username"]
        login_passwd = request.form["password"]
        with open(SETTINGS_FOLDER+"config.json") as f:
            data = json.load(f)
        if (login_uname == data['username']):
            print(data["username"])
            if (login_passwd == data['password']):
                print(data["password"])
                session['uname'] = request.form["password"]
                session['passwd'] = request.form["password"]
                return redirect('/home')
            else:
                return render_template('login/login.html')
        else:
            return render_template('login/login.html')
    else:
        return render_template('login/login.html')

@app.route("/logout")
def logout():
    session.pop('uname', None)
    session.pop('passwd', None)
    return redirect('/login')

@app.route("/home")
def home():
    try: 
        if (session['uname'] and session["passwd"]):
            return render_template("home/home.html")
    except:
        return redirect('/login')

# ROUTE SETTINGS
@app.route("/settings")
def settings():
    return render_template("settings/settings.html")

# ROUTE LOGS
@app.route("/logs", methods=['GET', 'POST'])
def readlogs():
    if (request.method == 'GET'):
        with open(SETTINGS_FOLDER + "logs.txt", 'r') as f:
            a = f.read()
            return a

# ROUTE SETTINGS
@app.route("/change_passwd", methods=["POST"])
def change_passwd():
    if (request.method == 'POST'):
        passwd_old = request.form["passwd-old"]
        passwd_new = request.form["passwd-new"]

        with open(SETTINGS_FOLDER+"config.json") as f:
            data = json.load(f)
        if (passwd_old == data['password']):
            print("password match")
            data['password'] = passwd_new
            with open(SETTINGS_FOLDER+"config.json", 'w') as f:
                f.write(json.dumps(data))
            return redirect('/logout')
        else:
            return redirect('/settings')
    else:
        return redirect('/settings')

# DOWNLOAD FILE
@app.route('/uploads/<path:filename>')
def download_file(filename):
    app.config['UPLOAD_FOLDER'] = CLOUD_FOLDER
    return send_from_directory(app.config["UPLOAD_FOLDER"], filename)

# LOGS
@app.route('/settings/logs')
def logs():
    app.config['SETTINGS_FOLDER'] = SETTINGS_FOLDER
    return send_from_directory(app.config["SETTINGS_FOLDER"], 'logs.txt')

# ----------------------------------------------------------#
# FILE MANIPULATION                                         #
# ----------------------------------------------------------#

# DIR LISTER
@app.route("/reader", methods=["GET", "POST"])
def reader():
    name = request.form['name']
    reader_folders = [ i for i in os.listdir(CLOUD_FOLDER+name) if os.path.isdir(os.path.join(CLOUD_FOLDER+name, i)) ]
    reader_folders.sort()

    reader_files = [ i for i in os.listdir(CLOUD_FOLDER+name) if os.path.isfile(os.path.join(CLOUD_FOLDER+name, i)) ]
    reader_files.sort()
    content = {'folder':reader_folders, 'files':reader_files}
    return content

# FILE WRITER
@app.route("/writer", methods=["GET", "POST"])
def writer():
    file_name = request.form['name']
    file_data = request.form['data']
    with open(CLOUD_FOLDER+file_name, 'w') as node:
        node.write(file_data)

# FILE READER
@app.route("/file_reader", methods=["GET", "POST"])
def file_reader():
    file_name = request.form['name']
    with open(CLOUD_FOLDER+file_name, 'r') as node:
        out = node.read()
        print(out)
    return out

# FILE MANIPULATION
@app.route("/actions", methods=["GET", "POST"])
def actions():
    # Delete File
    if (request.form['action'] == "delete"):
        delete_name = request.form['name']
        os.remove(CLOUD_FOLDER + delete_name)
    
    # Move File
    elif (request.form['action'] == "move"):
        move_name = request.form['name']
        move_location = request.form['location']
        os.rename(CLOUD_FOLDER+move_name, CLOUD_FOLDER+move_location)

    # Create File
    elif (request.form['action'] == "create"):
        node_name = request.form['name']
        node_type = request.form['type']
        if (node_type == "file"):
            with open(CLOUD_FOLDER+node_name, 'w') as node:
                node.write("")
        else:
            os.mkdir(CLOUD_FOLDER+node_name)
    
    return None

# ----------------------------------------------------------#
# EXECUTE                                                   #
# ----------------------------------------------------------#
if __name__ == '__main__':
    app.secret_key = secrets.token_urlsafe(32) 
    app.run(debug=True, host="0.0.0.0", port=1200)
    
