let currentGroup = ''; 
function msgBubble(msg, color) {
  return `
    <div class='message__bubble' data-group=${currentGroup} style='background-color: ${color}'>
      <label>${msg}</label>
    </div> <br>
  `
}

$.get("/logs", function(data, status){
  let dataList = data.split('\n');
  dataList.forEach(element => {
    $('.chat__messages').append(msgBubble(element, 'tan'))
    $('.chat__messages').scrollTop($('.chat__messages')[0].scrollHeight);
  });
});