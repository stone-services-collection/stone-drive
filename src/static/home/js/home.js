// CODE FOR FILE STORAGE
(function() {
let layout = 'row'
let currentDir = ""

// Folder Template
let templateFolder = (argLayout, argLabel, displayName) => {
  return `
    <div class='${argLayout} folder' data-type='folder' data-name='${argLabel}' name='x'>
      <img class="thumb__folder" src='/static/home/img/folder.png'>
      <label class='node__title'>${displayName}</label>
      <label class='metadata file__size'>3 KB</label>
      <label class='metadata meta__type '>Folder</label>
    </div>`
}

// FILE TEMPLATE
let templateFile = (argLayout,
                    argFileType,
                    path,
                    argFileName,
                    displayName,
                    showNoneVideo = "none",
                    showAudio = "none",
                    showImg = "none",
                    showText = "none",
                    showVideo = "none",
                    showPdf = "none",
                    fileIcon = "file_text.svg") => {
  return `
    <div class='${argLayout} file' data-type=${argFileType} data-name="${argFileName}" name='x'>
      <img class="thumb__video--not-working" style='display: ${showNoneVideo}' src='/static/home/img/file_icons/file_media_video.svg' />
      <img class="thumb__audio" style='display: ${showAudio}' src='/static/home/img/file_icons/file_media_audio.svg' />
      <img class="thumb__img" style='display: ${showImg}' src='/uploads${path}' />
      <video class='thumb__video--working' data-working="yes" style='display: ${showVideo}' preload="metadata">
        <source src="/uploads${path}#t=0.5" type="video/mp4">
      </video>
      <img class="thumb__file" style='display: ${showText}' src='/static/home/img/file_icons/${fileIcon}' />
      <img class="thumb__pdf" style='display: ${showPdf}' src='/static/home/img/file_icons/file_pdf.svg' />
      <label class='node__title'>${displayName}</label>
      <label class='metadata file__size'>3 KB</label>
      <label class='metadata meta__type file__type'>${argFileType}</label>
    </div> `
}

// DISPLAY FILE AND FOLDER ICONS
function listContent(list1, list2) {
  let fileName;
  let folderName;
  for(var i in list1) {
    folderName = list1[i]
    if (layout == "grid") {
      if(folderName.length > 13 ) {
        folderName = folderName.substring(0,5) + folderName.substring(-4, -1)
      }
    }
    $('.table').append(
      templateFolder(layout, list1[i], folderName)
    )
  }

  for(var i in list2) {
    let extentions = {}
    let a = list2[i].split('.').pop()
    let fIcon = "file_text.svg"

    fileName = list2[i];
    if (layout == "grid") {
      if(fileName.length > 13 ) {
        fileName = fileName.substring(0,5) + fileName.substring(-4, -1)
      }
    }

    // Only show icon in acordance with corisponding extention from file template
    if ('wmv' == a) { extentions['showNoneVideo'] = 'initial' }
    else if  ('mp3' == a || 'wav' == a ) { extentions['showMusic'] = 'initial' }
    else if ('jpg' == a || 'png' == a || 'jpeg' == a || 'svg' == a) { extentions['showImg'] = 'initial' }
    else if ('ogg' == a || 'mp4' == a || a == 'MP4') { extentions['showVideo'] = 'initial' }
    else if ('pdf' == a) {extentions['showPdf'] = 'initial'}
    else if ('c' == a) { a = "c"; extentions['showText'] = 'initial'; fIcon = "file_code_c.svg" }
    else if ('py' == a) { a = "python"; extentions['showText'] = 'initial'; fIcon = "file_code_py.svg" }
    else if ('js' == a) { a = "javascript"; extentions['showText'] = 'initial'; fIcon = "file_code_js.svg" }
    else if ('xml' == a) { a = "XML"; extentions['showText'] = 'initial'; fIcon = "file_code_xml.svg" }
    else if ('json' == a) { a = "JSON"; extentions['showText'] = 'initial'; fIcon = "file_code_json.svg" }
    else if ('c++' == a) { a = "Text"; extentions['showText'] = 'initial'; fIcon = "file_code_c++.svg" }
    else if ('html' == a) { a = "HTML"; extentions['showText'] = 'initial'; fIcon = "file_code_html.svg" }
    else { a = 'Text'; extentions['showText'] = 'initial'}
    
    $('.table').append(
      templateFile(layout,a,currentDir+list2[i], list2[i], fileName, extentions['showNoneVideo'], extentions['showMusic'], extentions['showImg'], extentions['showText'], extentions['showVideo'], extentions['showPdf'], fileIcon=fIcon )
    )
  }
}

// ADD NEW FILE
let imgNewFile = document.getElementsByClassName('img__newfile')[0]
imgNewFile.addEventListener('click', function() {
  let itemName = prompt("Enter Item Name")
  if (itemName != null) {
    $.post("/actions", {action: "create", type: 'file', name: `${currentDir}${itemName}`},
      function(result) {}
    )
  }
})

// ADD NEW FOLDER
let imgNewFolder = document.getElementsByClassName('img__newfolder')[0]
imgNewFolder.addEventListener('click', function() {
  let itemName = prompt("Enter Item Name")
  if (itemName != null) {
    $.post("/actions", {action: "create", type: 'folder', name: `${currentDir}${itemName}`},
      function(result) {}
    )
  }
})

// SWITCH LAYOUT GRID / LIST
let optionGrid = document.getElementsByClassName('option__grid')[0]
optionGrid.addEventListener('click', function() {
  $('.node__title').each(function() {
    if($(this).text().length > 13 ) {
      let juice = $(this).text().substring(0,5) + $(this).text().substring(-4, -1)
      console.log(juice)
      $(this).text(juice)
    }
  })
  $('.file').removeClass('row').addClass('grid')
  $('.folder').removeClass('row').addClass('grid')
  layout = 'grid'
})

let optionList = document.getElementsByClassName('option__list')[0]
optionList.addEventListener('click', function() {
  $('.node__title').each(function() {
    $(this).text($(this).parent().attr("data-name"))
  })
  $('.file').removeClass('grid').addClass('row')
  $('.folder').removeClass('grid').addClass('row')
  layout = 'row'
})

// COPY FILE
function copyPath() {
  window.prompt("Copy to clipboard: Ctrl+C, Enter", currentDir);
}

// WHEN FOLDER CLICKED
$('body').on('click', '.folder', function() {
  let files;
  let folders;
  let thisFolder = $(this)
  let folderName = ($(this).attr('data-name'))
  
  $('.table').empty()
  if ($(this).hasClass('navigation__folder')) {
    currentDir = $(this).attr("data-path")
    $(this).after().nextAll().remove()
  } else {
    currentDir += $(this).attr('data-name')+"/"
  }

  $.post("/reader", {name: currentDir},
    function(result, status){
      files = result['files']
      folders = result['folder']
      $('tr.node').remove()
      listContent(folders, files)
      if (thisFolder.attr('name')) {
        $('.navigation__path').append(`
          <label class='folder navigation__folder' data-type='folder' data-path='${currentDir}'>
            <i class='node__title'>${folderName}</i>
          </label>
        `)
      }
    }
  );
})

// WHEN FILE CLICKED
$('body').on('click', '.file', function() {
  let fileName = $(this).attr("data-name")
  let fileType = $(this).find('.file__type').text()
  switch (fileType) {
    case 'HTML':
    case 'XML':
    case 'javascript':
    case 'Text':
      $('.text__filename').text(fileName)
      $.post("/file_reader", {name: `${currentDir}${fileName}`},
        function(result) {
          $('.view__textarea').show()
          $('.nnn').val(result)
          $('.table').show()
        }
      )
      break;
    case 'jpg':
    case 'png':
    case 'jpeg':
    case 'svg':
      $('.image__filename').text(fileName)
      $('.view__images').show()
      $('.view__images img').attr('src', `/uploads${currentDir}${fileName}`)
      break;
    case 'mp4':
    case 'MP4':
    case 'mp3':
    case 'ogg':
      $('.video__filename').text(fileName)
      $('.view__video').show()
      $(".video__player").attr("src", `/uploads${currentDir}${fileName}`)
      break
    case 'wmv':
      $('.clouds').removeClass("effect--faded")
      window.open(`/uploads${currentDir}${fileName}`);
      break
  }
})

// Home Folder
let navHome = document.getElementsByClassName("nav__home")[0]
navHome.addEventListener('click', function() { driveHome() })

function driveHome() {
  currentDir = "/"
  $('.navigation__path').empty()
  $('.table').empty()

  $.post("/reader", {name: ''},
    function(result, status){
      files = result['files']
      folders = result['folder']
      $('.node').remove()
      listContent(folders, files)
    }
  );
}

$('.menu__toggle').on('click', function() {
  $('.view__selection').toggle()
  $('.view__side').hide()
})

$('.arrow__toggle').on("click", function() {
  $('.view__side').toggle()
})


// RIGHT-CLICK MENU
function optionsMenu(e, obj) {
  let fileNode = obj
  let file = currentDir+obj.find('.node__title').text()
  let fileText = obj.find('.node__title').text()
  $('.options').css({"left": e.clientX, "top": e.clientY}).show()
  e.preventDefault();
  
  $(".options__rename").click(function() {
    let newName = prompt("Enter New Name")
    $.post("/rename", {name: file, newName: newName},
      function(result, status){console.log(result)}
    )
  })

  $(".options__move").click(function() {
    let location = prompt("Enter Location")
    $.post("/actions", {action: 'move', name: `${currentDir}${fileText}`, location: location+fileText})
  })
  $('.options__trash').click(function() {
    $.post("/actions", {action: 'delete', name: file})
    fileNode.remove()
  })
}

 // RIGHT-CLICK FILE
$(document).on("contextmenu", '.file', function(e) {
  optionsMenu(e, $(this))
}).on("contextmenu", '.folder', function(e) {
  optionsMenu(e, $(this))
}).on("contextmenu", '.explorer__content', function(e) {
  optionsMenu(e, $(this))
})

$("body").click(function() {
  $(".options").hide()
})

$('.menu-item').on('click', function() {})

// CODE FOR TEXTEDITOR
driveHome()

$('.text__close').click(function() {
  $('.view__textarea').hide()
  $('.table').show()
})

$('.text__save').click(function() {
  let fileName = $('.text__filename').text()
  let fileData = $('.nnn').val()
  console.log(fileData)
  console.log("hi")
  $.post("/writer", {name: `${currentDir}${fileName}`, data:`${fileData}`})
})

$(".image__close").on("click", function() {
  $(".view__images").hide()
})
$(".video__close").on("click", function() {
  $(".view__video").hide()
})
})()