# Stone Drive
Stone Drive is a home file storage application built on Python's Flask Web Framework

# Execution
1. <code>cd</code> into the stone-drive directory
2. run <code>./setup.sh</code>
3. the service will begin running on <code>localhost:1200</code>

# Structure / Environment 
Dashboard is targed at Ubuntu Server LTS, but can be modified for any Linux Distro, including Windows and BSD.

### Code Base
- **Python** - Application server code
- **Bash** - Setup script for installing dependencies and configuring enviornment
- **HTML/CSS** - Web UI structure and design

### Hierarchy
- **/src** - Location for source code 
- **/src/static** - Location for CSS, JS, and IMG files
- **/src/templates/** - Location for HTML files
- **/src/system/** - Local system utilites 
