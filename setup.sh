# Setup for Stone Drive

app_install () {
	echo "Installing for Debian/Ubuntu"

	# Debian/Ubuntu
	echo "Installing python..."
	sudo apt install python2 -y &> /dev/null
	sudo apt install python3 -y &> /dev/null
	
	echo "Installing pip..."
	sudo apt install python3-pip -y &> /dev/null
	
	echo "Installing Flask..."
	pip3 install flask &> /dev/null
	
	echo "=== Starting Dashboard Server =="
	cd src && python3 server.py
}

app_run () {

}

if [ $1 == "-install" ]; then
	app_setup
elif [ $1 == "-run" ]; then
	app_run
else
	echo "Error: Unknown Parameter"
fi
